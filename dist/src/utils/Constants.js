const STATUS_CODE = {
    ERROR: 500,
    NOT_FOUND: 404,
    NO_CONTENT: 204,
    SUCCESS: 200,
    BAD_REQUEST: 400,
    CONFLICT: 409,
    UNAUTHORIZED: 401,
};
export { STATUS_CODE as statusCode, };
//# sourceMappingURL=Constants.js.map
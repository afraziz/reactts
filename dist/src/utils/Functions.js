const getStorage = (key) => {
    return localStorage.getItem(key);
};
const setStorage = (key, value) => {
    return localStorage.setItem(key, value);
};
const destroyStorage = (key) => {
    return localStorage.removeItem(key);
};
const getSessionStorage = (key) => {
    return sessionStorage.getItem(key);
};
const setSessionStorage = (key, value) => {
    return sessionStorage.setItem(key, value);
};
const destroySessionStorage = (key) => {
    return sessionStorage.removeItem(key);
};
const checkImageExists = (url, callback) => {
    const img = new Image();
    img.onload = function () {
        callback(true);
    };
    img.onerror = function () {
        callback(false);
    };
    img.src = url;
};
export { getStorage, setStorage, destroyStorage, getSessionStorage, setSessionStorage, destroySessionStorage, checkImageExists, };
//# sourceMappingURL=Functions.js.map
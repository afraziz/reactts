import React from 'react';
import { createStore, combineReducers } from 'redux';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createGlobalStyle } from 'styled-components';
import RootStackNavigator from './components/navigation/RootStackNavigator';
import { setLocale } from './actions';
import reducers from './reducers';
import { setStore } from './utils/Locale';
const GlobalStyle = createGlobalStyle `
  body {
    background-color: rgb(255,254,252);
  }
`;
const store = createStore(combineReducers(Object.assign({}, reducers)));
setStore(store);
if (navigator) {
    const userLang = navigator.language;
    if (store.getState().locale !== userLang) {
        store.dispatch(setLocale(userLang));
    }
}
ReactDOM.render(React.createElement(Provider, { store: store },
    React.createElement(GlobalStyle, null),
    React.createElement(RootStackNavigator, null)), document.getElementById('app'));
//# sourceMappingURL=index.js.map
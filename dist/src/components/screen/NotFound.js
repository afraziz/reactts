import React, { Component } from 'react';
import Button from '../shared/Button';
import styled from 'styled-components';
const Container = styled.div `
  display: 'flex',
  justify-content: 'center',
`;
class NotFound extends Component {
    render() {
        return (React.createElement(Container, null,
            React.createElement(Button, { onClick: () => this.props.history.goBack(), txt: 'back to tab page' })));
    }
}
export default NotFound;
//# sourceMappingURL=NotFound.js.map
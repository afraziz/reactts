import React, { Component } from 'react';
import styled from 'styled-components';
import { WhiteButton, DarkButton } from '../ui/Buttons';
const Text = styled.span `
  font-size: 14px;
  color: rgb(128, 109, 216);
`;
const LogoImg = styled.img `
  position: absolute;
  left: 16px;
  height: 20px;
  width: 20px;
  object-fit: cover
`;
export class Button extends Component {
    render() {
        if (this.props.white) {
            return (React.createElement(WhiteButton, { onClick: () => this.props.onClick() },
                this.props.imgSrc
                    ? React.createElement(LogoImg, { src: this.props.imgSrc, srcSet: this.props.srcset })
                    : null,
                React.createElement(Text, null, this.props.txt)));
        }
        return (React.createElement(DarkButton, { onClick: () => this.props.onClick() },
            this.props.imgSrc
                ? React.createElement(LogoImg, { src: this.props.imgSrc, srcSet: this.props.srcset })
                : null,
            React.createElement(Text, null, this.props.txt)));
    }
}
export default Button;
//# sourceMappingURL=Button.js.map
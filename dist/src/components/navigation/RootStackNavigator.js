import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Intro from '../screen/Intro';
import NotFound from '../screen/NotFound';
class RootStackNavigator extends Component {
    render() {
        return (React.createElement(BrowserRouter, null,
            React.createElement("div", { style: { textAlign: 'center' } },
                React.createElement(Switch, null,
                    React.createElement(Route, { exact: true, path: '/', component: Intro }),
                    React.createElement(Route, { component: NotFound })))));
    }
}
export default RootStackNavigator;
//# sourceMappingURL=RootStackNavigator.js.map
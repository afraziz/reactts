import { css } from 'styled-components';
export const theme = {
    mainColor: '#3498db',
    dangerColor: '#e74c3c',
    successColor: '#2ecc71',
};
const sizes = {
    mobile: 768,
};
export const media = Object.keys(sizes).reduce((acc, label) => {
    acc[label] = (literals, ...placeholders) => css `
        @media (max-width: ${sizes[label]}px) {
          ${css(literals, ...placeholders)};
        }
      `.join('');
    return acc;
}, {});
//# sourceMappingURL=theme.js.map
const initialState = {
    locale: 'en',
};
function locale(state = initialState.locale, action) {
    switch (action.type) {
        case 'UPDATE_LOCALE':
            return action.value;
        default:
            return state;
    }
}
const myReducers = {
    locale,
};
export default myReducers;
//# sourceMappingURL=index.js.map
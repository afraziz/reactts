const SET_LOCALE = 'SET_LOCALE';
export function setLocale(lang) {
    return {
        type: SET_LOCALE,
        value: lang,
    };
}
//# sourceMappingURL=index.js.map
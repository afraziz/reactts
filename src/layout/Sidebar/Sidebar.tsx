import { Link } from 'react-router-dom'
import { SidebarWrapper } from './styled'
import { SidebarMenu } from './SidebarMenu'

const logo = require('../../images/react_logo.png')
export const Sidebar = () => {
  return (
    <SidebarWrapper>
      <div>
        <div className="sidebar-logo-top">
          <Link to="/">
            <img src={logo}/>
          </Link>
        </div>
        <SidebarMenu />
      </div>
    </SidebarWrapper>
  )
}

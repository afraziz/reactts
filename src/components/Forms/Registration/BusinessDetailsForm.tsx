import PropTypes from 'prop-types'
import { useState } from 'react'
import { Form, Formik } from 'formik'
import { HeadingLarge } from 'styles/general-styles'
import { Submit ,TextInput} from 'components/FormFields'
import * as Yup from 'yup'
import { HelpModal } from 'components/Modals'
import { connect } from 'react-redux'
import { updateProgress, register } from '../../../features//registration/registrationActions'
import { toast } from 'react-toastify'

const BusinessDetailsForm = ({ registration, updateProgress, register }) => {
  const [isModalOpen, setModalIsOpen] = useState(false)

  const handleSubmit = async (values) => {
    const formData = values
    formData.supplier_code = registration.supplierCode

    const res = await register(values)
    if (res.success === true) {
      if (res.meta.access_token == null) {
        updateProgress(registration.progressStep + 5)
      } else {
        updateProgress(registration.progressStep)
      }
    } else if (res?.errors?.supplier_code) {
      updateProgress(registration.progressStep - 1)
      toast.error(res.errors.supplier_code)
    } else if (res.errors.email) {
      toast.error(res.errors.email[0])
    } else {
      toast.error(res.message)
    }
  }

  const validationSchema = Yup.object({
    email: Yup.string().email().required('Email is required'),
    mobile: Yup.string()
      .required('Mobile Number is Required')
      .matches(
        /^(?=.*)((?:\+?61) ?(?:\((?=.*\)))?([2-47-8])\)?|(?:\((?=.*\)))?([0-1][2-47-8])\)?) ?-?(?=.*)((\d{1} ?-?\d{3}$)|(00 ?-?\d{4} ?-?\d{4}$)|( ?-?\d{4} ?-?\d{4}$)|(\d{2} ?-?\d{3} ?-?\d{3}$))/,
        'Invalid Phone No.'
      ),
  })

  const initialValues = {
    email: registration.email,
    mobile: registration.phoneNumber,
  }
  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {(props) => {
          const { isSubmitting } = props
          return (
            <Form>
              <h4>Step 2</h4>
              <HeadingLarge uppercase>
                Enter your BUSINESS CONTACT details
              </HeadingLarge>
              <p className="mb-0">
                Please provide your existing email address that is registered to
                your Harvey Beef supplier account.
              </p>
              <div className="row">
                <div className="col-12">
                  <TextInput
                    labelMargin="16px"
                    label="Email"
                    required={false}
                    name="email"
                    type="email"
                  />
                </div>
                <div className="col-12">
                  <TextInput
                    label="Mobile Number"
                    required={false}
                    name="mobile"
                    type="tel"
                  />
                </div>
                <div className="col-12">
                  <Submit
                    type="submit"
                    marginTop="26px"
                    text="Continue"
                    uppercase
                    submittingText="Processing..."
                    widthExpand
                    isSubmitting={isSubmitting}
                    refineResults={false}
                  />
                </div>
                <div className="col-12">
                  <p className="signup mb-0">
                    Having trouble? &nbsp;
                    <span
                      className="click-here-link"
                      role={'button'}
                      onClick={() => {
                        setModalIsOpen(true)
                      }}
                    >
                      Talk to our team
                    </span>
                  </p>
                </div>
              </div>
            </Form>
          )
        }}
      </Formik>
      <HelpModal isOpen={isModalOpen} setModalIsOpen={setModalIsOpen} />
    </>
  )
}

BusinessDetailsForm.propTypes = {
  isSubmitting: PropTypes.func,
  register: PropTypes.func,
  registration: PropTypes.shape({
    email: PropTypes.any,
    phoneNumber: PropTypes.any,
    progressStep: PropTypes.number,
    supplierCode: PropTypes.any,
  }),
  updateProgress: PropTypes.func,
}

const mapStateToProps = (state) => ({
  registration: state.registration,
})

export default connect(mapStateToProps, { updateProgress, register })(
  BusinessDetailsForm
)

import PropTypes from 'prop-types'
import { useState } from 'react'
import { Form, Formik } from 'formik'
import { HeadingLarge } from 'styles/general-styles'
import { Checkbox, Submit, TextInput } from 'components/FormFields'
import * as Yup from 'yup'
import { HelpModal } from 'components/Modals/HelpModal'
import { connect } from 'react-redux'
import {
  updateProgress,
  updatePassword,
} from '../../../features/registration/registrationActions'
import { toast } from 'react-toastify'

const CreatePasswordForm = ({
  updateProgress,
  registration,
  updatePassword,
}) => {
  const [isModalOpen, setModalIsOpen] = useState(false)
  const handleSubmit = async (values) => {
    const res = await updatePassword(registration.userId, values)
    if (res.success === true) {
      updateProgress(registration.progressStep)
    } else {
      toast.error(res.message)
    }
  }

  const validationSchema = Yup.object({
    password: Yup.string()
      .required('Password is required')
      .min(8, 'Your password must be more than 8 characters.')
      .matches(
        // eslint-disable-next-line no-useless-escape
        /^(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,20})/,
        'Rules i.e. 1 character, 1 capital alphabet, 1 number'
      ),
    password_confirmation: Yup.string().oneOf(
      [Yup.ref('password'), null],
      'Your password must match!'
    ),
    is_agree: Yup.boolean().oneOf(
      [true],
      'You must accept the terms and conditions'
    ),
  })

  const initialValues = {
    password: '',
    password_confirmation: '',
    is_agree: false,
  }
  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {(props) => {
          const { setFieldValue, isSubmitting } = props
          return (
            <Form>
              <h4>Step 4</h4>
              <HeadingLarge uppercase>
                Create A password for your account
              </HeadingLarge>
              <p className="mb-0">
                Pellentesque ut mauris potenti tempus proin consequat laoreet.
                Eget quis dolor nec enim. Adipiscing adipiscing.
              </p>
              <div className="row">
                <div className="col-12">
                  <TextInput
                    labelMargin="16px"
                    label="Enter Password"
                    required={false}
                    name="password"
                    type="password"
                  />
                </div>
                <div className="col-12">
                  <TextInput
                    label="Re-Enter Password"
                    required={false}
                    name="password_confirmation"
                    type="password"
                  />
                </div>
                <div className="col-12 mt-5 mb-2">
                  <Checkbox
                    label="I agree with the"
                    highlighted_label="Terms & Conditions"
                    name="is_agree"
                    display="inline-block"
                    handleChange={setFieldValue}
                  />
                </div>
                <div className="col-12">
                  <Submit
                    type="submit"
                    marginTop="20px"
                    text="Create My Account"
                    uppercase
                    submittingText="Processing..."
                    widthExpand
                    isSubmitting={isSubmitting}
                    refineResults={false}
                  />
                </div>
              </div>
            </Form>
          )
        }}
      </Formik>
      <HelpModal isOpen={isModalOpen} setModalIsOpen={setModalIsOpen} />
    </>
  )
}

CreatePasswordForm.propTypes = {
  registration: PropTypes.shape({
    progressStep: PropTypes.any,
    userId: PropTypes.any,
  }),
  setFieldValue: PropTypes.func,
  updatePassword: PropTypes.func,
  updateProgress: PropTypes.func,
  isSubmitting: PropTypes.func,
}

const mapStateToProps = (state) => ({
  registration: state.registration,
})
export default connect(mapStateToProps, { updateProgress, updatePassword })(
  CreatePasswordForm
)

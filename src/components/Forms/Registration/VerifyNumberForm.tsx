import PropTypes from 'prop-types'
import { useState } from 'react'
import { Form, Formik } from 'formik'
import { HeadingLarge } from 'styles/general-styles'
import { Submit } from 'components/FormFields'
import * as Yup from 'yup'
import { ReceiveYourCode } from 'components/Modals'
import { connect } from 'react-redux'
import {
  updateProgress,
  verifyMobileNumber,
} from '../../../features/registration/registrationActions'
import { toast } from 'react-toastify'

const VerifyNumberForm = ({
  updateProgress,
  registration,
  verifyMobileNumber,
}) => {
  const [isModalOpen, setModalIsOpen] = useState(false)

  const numberPreview =
    registration.mobile.slice(0, 3) +
    registration.mobile.slice(-3).padStart(registration.mobile.length - 3, '*')

  const handleSubmit = async (values) => {
    const res = await verifyMobileNumber(registration.userId, values)

    if (res.success === true) {
      updateProgress(registration.progressStep)
    } else {
      toast.error('Invalid code.')
    }
  }
  const validationSchema = Yup.object({
    two_fa_token: Yup.string()
      .required('Verification Code is required')
      .matches(/^[0-9]{6}$/, 'Must be exactly 6 characters'), // .matches('1234',"Verification Code is Incorrect")
  })

  const initialValues = {
    two_fa_token: '',
  }
  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {(props) => {
          const { setFieldValue, isSubmitting } = props
          return (
            <Form>
              <h4>Step {registration.progressStep}</h4>
              <HeadingLarge uppercase>GOT A CODE</HeadingLarge>
              <p>
                You will get a confirmation code via SMS to {numberPreview},
                please enter the code below in order to continue:
              </p>
              <div className="col-12 p-0">
                <p className="receive-your-code mb-0">
                  <span
                    role={'button'}
                    onClick={() => {
                      setModalIsOpen(true)
                    }}
                  >
                    <u>Didn’t receive your code?</u>
                  </span>
                </p>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <Submit
                    type="submit"
                    marginTop="26px"
                    text="Continue"
                    isSubmitting={isSubmitting}
                    uppercase
                    submittingText="Processing..."
                    widthExpand
                    refineResults={false}
                  />
                </div>
              </div>
            </Form>
          )
        }}
      </Formik>
      <ReceiveYourCode isOpen={isModalOpen} setModalIsOpen={setModalIsOpen} />
    </>
  )
}
VerifyNumberForm.propTypes = {
  isSubmitting: PropTypes.func,
  registration: PropTypes.shape({
    mobile: PropTypes.string,
    progressStep: PropTypes.any,
    userId: PropTypes.any,
  }),
  setFieldValue: PropTypes.func,
  updateProgress: PropTypes.func,
  verifyMobileNumber: PropTypes.func,
}
const mapStateToProps = (state) => ({
  registration: state.registration,
})

export default connect(mapStateToProps, { updateProgress, verifyMobileNumber })(
  VerifyNumberForm
)

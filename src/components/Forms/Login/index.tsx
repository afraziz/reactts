import PropTypes from 'prop-types'
import { Form, Formik } from 'formik'
import React from 'react'

import { Link, useNavigate } from 'react-router-dom'
import * as Yup from 'yup'
import { Checkbox, Submit, TextInput } from '../../FormFields'

export const LoginForm = ({ signin }) => {
  const navigate = useNavigate()

  const handleSubmit = () => {
    signin()
    localStorage.setItem('is_logged_in', "true")
    navigate('../account-details')
  }

  const validationSchema = Yup.object({
    email: Yup.string().email().required().label('Email'),
    password: Yup.string().required().label('Password'),
  })

  const initialValues = {
    email: '',
    password: '',
    remember: false,
  }
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={handleSubmit}
    >
      {(props) => {
        const { isSubmitting, setFieldValue } = props

        return (
          <Form>
            <div className="row">
              <div className="col-sm-12">
                <TextInput label="Email" name="email" type="email" />
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <TextInput label="Password" name="password" type="password" />
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <div className="d-flex flex-row justify-content-between mt-4">
                  <Checkbox
                    label="Remember me"
                    name="remember"
                    display="inline-block"
                    handleChange={setFieldValue}
                  />
                  <Link
                    className="forgot-pass"
                    to="/forgot-password"
                  >
                    Forgot your password?
                  </Link>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-sm-12">
                <Submit
                  type="submit"
                  isSubmitting={isSubmitting}
                  marginTop="26px"
                  text="Sign in"
                  uppercase
                  submittingText="Processing..."
                  widthExpand
                  refineResults={false}
                />
              </div>
              <div className="col-12">
                <p className="signup mb-0">
                  Don’t have an account?&nbsp;
                  <Link className="" to="/signup">
                    Sign up here
                  </Link>
                </p>
              </div>
            </div>
          </Form>
        )
      }}
    </Formik>
  )
}

LoginForm.propTypes = {
  isSubmitting: PropTypes.func,
  signin: PropTypes.func,
  setFieldValue: PropTypes.func,
}

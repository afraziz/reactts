import PropTypes from 'prop-types'
import { Form, Formik } from 'formik'
import * as Yup from 'yup'
import { Submit, TextInput } from '../../FormFields'

export const ResetPasswordForm = ({ handleNavigation }) => {
  const handleSubmit = (values) => {
    if (values) {
      handleNavigation()
    }
  }

  const validationSchema = Yup.object({
    password: Yup.string()
      .required('Password is required')
      .min(8, 'Your password must be more than 8 characters.')
      .matches(
        // eslint-disable-next-line no-useless-escape
        /^(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,20})/,
        'Rules i.e. 1 character, 1 capital alphabet, 1 number'
      ),
    confirm_password: Yup.string().oneOf(
      [Yup.ref('password'), null],
      'Your password must match!'
    ),
  })

  const initialValues = {
    password: '',
    confirm_password: '',
  }
  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {(props) => {
          const { isSubmitting } = props

          return (
            <Form>
              <div className="row">
                <div className="col-sm-12">
                  <TextInput
                    label="Enter new password"
                    name="password"
                    type="password"
                  />
                </div>
                <div className="col-sm-12">
                  <TextInput
                    label="Confirm new password"
                    name="confirm_password"
                    type="password"
                  />
                </div>
              </div>

              <div className="row">
                <div className="col-sm-12">
                  <Submit
                    type="submit"
                    isSubmitting={isSubmitting}
                    marginTop="26px"
                    text="Sign in"
                    uppercase
                    submittingText="Processing..."
                    widthExpand
                    refineResults={false}
                  />
                </div>
              </div>
            </Form>
          )
        }}
      </Formik>
    </>
  )
}

ResetPasswordForm.propTypes = {
  handleNavigation: PropTypes.func,
  isSubmitting: PropTypes.func,
}

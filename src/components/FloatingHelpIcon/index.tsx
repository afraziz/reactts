import PropTypes from 'prop-types';
import { MdCall } from 'react-icons/md'
import { FloatingHelpIconStyled } from './styled'

export function FloatingHelpIcon({ isModalOpen, setModalIsOpen }) {
  return (
    <FloatingHelpIconStyled onClick={() => setModalIsOpen(!isModalOpen)}>
      <MdCall />
      <span>Help</span>
    </FloatingHelpIconStyled>
  )
}
FloatingHelpIcon.defaultProps = {
  isModalOpen: false,
  setModalIsOpen: () => {},
}

FloatingHelpIcon.propTypes = {
  isModalOpen: PropTypes.bool,
  setModalIsOpen: PropTypes.func,
}

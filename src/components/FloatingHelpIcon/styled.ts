import styled from 'styled-components'

export const FloatingHelpIconStyled = styled.button`
  display: none;
  padding: 13px;
  padding-left: 20px;
  position: fixed;
  bottom: 35px;
  right: 23px;
  border: none;
  border-radius: 50px;
  background: ${(props) => props.theme.colors.hbGreyBackground};
  color: ${(props) => props.theme.colors.hbTextBlue};
  font-size: 16px;
  cursor: pointer;
  z-index: 5;
  svg {
    margin-right: 8px;
    font-size: 22px;
    color: ${(props) => props.theme.colors.hbTextGreyDark};
  }
  ${(props) => props.theme.mediaQueries.md} {
    display: block;
  }
`

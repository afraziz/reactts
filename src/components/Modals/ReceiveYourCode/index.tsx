import PropTypes from 'prop-types'
import { BaseModal } from 'components/Modals/BaseModal'
import { Button } from 'components/Button'
import { ButtonOutline } from 'components/Button/ButtonOutline'
import { RiQuestionLine } from 'react-icons/ri'
import { ReceiveYourCodeStyled } from './styled'

export const ReceiveYourCode = ({ isOpen, setModalIsOpen }) => {
  return (
    <ReceiveYourCodeStyled>
      <BaseModal
        hideClose
        isOpen={isOpen}
        width="510px"
        onRequestClose={() => {
          setModalIsOpen(false)
        }}
        content={''}
        scroll={false}
        backgroundcolor={''}
      >
        <div className="question-icon">
          <RiQuestionLine color="#92400E" />
        </div>
        <h3>Didn’t receive your code?</h3>
        <p>
          We can help! First, check that you have confirmed the right mobile
          number. If you’re on Wi-Fi, make sure that your device is connected to
          a mobile network signal.
        </p>
        <p>
          In some cases, a two-factor authentication (2FA) code can take a few
          minutes to arrive. If you don’t receive a code within five minutes,
          please get in touch with our team.
        </p>
        <div className="footer">
          <Button
            onClick={() => {
              setModalIsOpen(false)
            }}
            width="50%"
          >
            Close
          </Button>

          <ButtonOutline width="50%">Resend Code (30)</ButtonOutline>
        </div>
      </BaseModal>
    </ReceiveYourCodeStyled>
  )
}

ReceiveYourCode.propTypes = {
  isOpen: PropTypes.any,
  setModalIsOpen: PropTypes.func,
}

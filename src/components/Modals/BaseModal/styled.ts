import styled, { createGlobalStyle } from 'styled-components/macro'
import ReactModal from 'react-modal'

export const ModalTransitionStyles = createGlobalStyle`
    .ReactModal__Overlay {
        opacity: 0;
        transition: opacity 200ms ease-in-out;
        overflow-y: scroll;
    }
    .ReactModal__Overlay--after-open {
        opacity: 1;
    }
    .ReactModal__Overlay--before-close {
        opacity: 0;
    }
    .ReactModal__Content {
        margin-top: 45px;
        margin-bottom: 45px;
        top: 0;
        outline: none;
        height: 100%;
        maxHeight: 100%;
        max-width: 100% !important;
        ${(props) => props.theme.mediaQueries.sm} {
          max-width: 100% !important;
        }
        ${(props) => props.theme.mediaQueries.md} {
            max-width: 510px !important;
            height: auto;
            maxHeight: 'calc(100% - 60px)';
        }
    }
`

export const ModalInner = styled.div`
  transition: all 500ms ease;
  transform: translateY(${(props) => (props.isOpen ? '0' : '5px')});
  opacity: ${(props) => (props.isOpen ? '1' : '0')};
  overflow-y: ${(props) => (props.scroll ? 'auto' : 'initial')};
  position: relative;
  background-color: ${(props) =>
    props.backgroundcolor ? props.backgroundcolor : 'white'};
  border-radius: 0px !important;
  height: 100% !important;
  padding-top: 40px !important;
  &:focus-visible {
    outline-width: 0;
  }

  @keyframes modalSlide {
    from {
      transform: translateY(25px);
      opacity: 0;
    }

    to {
      transform: translateY(0);
      opacity: 1;
    }
  }

  animation-name: modalSlide;
  animation-duration: 500ms;

  border-radius: 8px;
  padding: 24px;
  color: ${(props) => props.theme.colors.hbTextGreyLight};
  font-size: 16px;
  .call-icon,
  .question-icon,
  .email-logo-icon {
    margin-bottom: 20px;
    padding: 10px;
    font-size: 26px;
    border-radius: 50%;
    width: 48px;
    height: 48px;
    background: ${(props) => props.theme.colors.hbGreyBackground};
    color: ${(props) => props.theme.colors.hbTextGreyDark};
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .question-icon {
    background: #ffe7b8;
  }
  .email-logo-icon {
    background: #b5dc79;
  }
  h3 {
    font-size: 26px;
    font-weight: 800;
    color: ${(props) => props.theme.colors.hbTextBlue};
    text-transform: uppercase;
    margin-bottom: 10px;
    font-family: Cervo Neue Bold;
  }
  p,
  ul {
    line-height: 24px;
  }
  ul {
    margin-bottom: 20px;
    padding-left: 30px;
  }
  .footer {
    display: block;
    button {
      &:first-of-type {
        margin-bottom: 12px;
      }
    }
  }
  ${(props) => props.theme.mediaQueries.md} {
    border-radius: 8px !important;
    padding-top: 24px !important;
    height: auto !important;
    .footer {
      display: flex;
      flex-direction: row;
      button {
        &:first-of-type {
          margin-right: 12px;
          margin-right: 12px;
          margin-right: 12px;
        }
      }
    }
  }
`

export const CloseButton = styled.button`
  position: absolute;
  right: 3px;
  top: 6px;
  background: none;
  border: none;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  z-index: 10;

  &:focus {
    outline: none;
  }
`
export const ModalHeaderWrapper = styled.div`
  transition: all 500ms ease;
  background: white;
  padding: 15px 26px 15px 26px;
  border-bottom: 1px solid #cfd1d8;
  display: flex;
  justify-content: space-between;
  .mobile-close-icon {
    background: none;
    border: none;
  }
  .mobile-help-icon {
    background: none;
    border: none;
    font-size: 22px;
  }
  ${(props) => props.theme.mediaQueries.md} {
    transition: all 500ms ease;
    display: none;
  }
`

export const ModalStyled = styled(ReactModal)`
  .modal-inner-content {
    width: 100% !important;
    margin: auto;
  }
  ${(props) => props.theme.mediaQueries.sm} {
    .modal-inner-content {
      width: ${(props) => (props.width ? props.width : '410px')} !important;
      margin: auto;
    }
  }
`

import PropTypes from 'prop-types'
import ReactModal from 'react-modal'
import { CloseIcon, CallIcon } from 'images/svg/index'
import {
  ModalStyled,
  ModalInner,
  ModalTransitionStyles,
  CloseButton,
  ModalHeaderWrapper,
} from './styled'

ReactModal.setAppElement('#root')

export function BaseModal({
  children,
  isOpen,
  content,
  width,
  scroll,
  hideClose,
  shouldCloseOnOverlayClick = true,
  shouldCloseOnEsc = true,
  backgroundcolor,
  ...rest
}) {
  const modalStyles = {
    overlay: {
      position: 'fixed',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      backgroundColor: 'rgba(36, 36, 36, 0.75)',
      zIndex: 999,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    content: {
      position: 'relative',
      margin: '0 auto',
      width: '100%',
      maxWidth: `${width || '500px'}`,
    },
  }

  return (
    <>
      <ModalTransitionStyles width={width} />
      <ModalStyled
        {...rest}
        isOpen={isOpen}
        closeTimeoutMS={500}
        style={modalStyles}
        shouldCloseOnOverlayClick={shouldCloseOnOverlayClick}
        shouldCloseOnEsc={shouldCloseOnEsc}
        width={width && '100%'}
      >
        {!hideClose && (
          <CloseButton isOpen={isOpen} onClick={() => rest.onRequestClose()}>
            <CloseIcon />
          </CloseButton>
        )}

        <ModalHeaderWrapper className="mobile-modal-header">
          <button
            className="mobile-close-icon"
            onClick={() => rest.onRequestClose()}
          >
            <CloseIcon />
          </button>
          <button
            className="mobile-help-icon"
            onClick={() => rest.onRequestClose()}
          >
            <CallIcon />
          </button>
        </ModalHeaderWrapper>
        <ModalInner
          isOpen={isOpen}
          scroll={scroll}
          backgroundcolor={backgroundcolor}
        >
          <div className="modal-inner-content">{children}</div>
        </ModalInner>
      </ModalStyled>
    </>
  )
}

BaseModal.propTypes = {
  backgroundcolor: PropTypes.any,
  children: PropTypes.any,
  content: PropTypes.any,
  hideClose: PropTypes.any,
  isOpen: PropTypes.any,
  scroll: PropTypes.any,
  shouldCloseOnEsc: PropTypes.bool,
  shouldCloseOnOverlayClick: PropTypes.bool,
  width: PropTypes.string,
}


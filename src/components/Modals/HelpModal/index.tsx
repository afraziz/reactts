import PropTypes from 'prop-types'
import { IoIosCall } from 'react-icons/io'
import { BaseModal } from 'components/Modals/BaseModal'
import { Button } from 'components/Button'
import { ButtonOutline } from 'components/Button/ButtonOutline'
import { HelpModalStyled } from './styled'

export function HelpModal({ isOpen, setModalIsOpen }) {
  return (
    <HelpModalStyled>
      <BaseModal
        hideClose
        isOpen={isOpen}
        width="510px"
        content={''}
        scroll={false}
        backgroundcolor={''}

        onRequestClose={() => {
          setModalIsOpen(false)
        }}
      >
        <div className="call-icon">
          <IoIosCall />
        </div>
        <h3>Talk to our team on +61 8 9729 0000</h3>
        <p>
          We’re here to support suppliers on the land and online. Give us a bell
          for assistance. <b>Contact us if:</b>
        </p>
        <ul>
          <li>You need help to create a supplier account</li>
          <li>Can’t find your supplier ID</li>
          <li>Your mobile number or email address is incorrect</li>
          <li>You just need some extra help</li>
        </ul>
        <div className="footer">
          <Button
            onClick={() => {
              setModalIsOpen(false)
            }}
          >
            Close
          </Button>

          <ButtonOutline>
            <a href="tel:+61897290000">
              <IoIosCall /> +61 8 9729 0000
            </a>
          </ButtonOutline>
        </div>
      </BaseModal>
    </HelpModalStyled>
  )
}

HelpModal.propTypes = {
  isOpen: PropTypes.any,
  setModalIsOpen: PropTypes.func,
}

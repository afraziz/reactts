import PropTypes from 'prop-types'
import { NotificationIcon } from '../../images/svg'
import { Avatar } from '../../components/Avatar'
import { Button } from '../../components/Button'
import { Link } from 'react-router-dom'
import { HiPlusCircle } from 'react-icons/hi'
import { Breadcrumb } from '../../components/Breadcrumb'
import { TopNavStyled } from './styled'

const av1 = require('../../images/avatars/av-1.jpeg')
const av2 = require('../../images/avatars/av-2.jpeg')
const av3 = require('../../images/avatars/av-3.jpeg')

export const TopNav = ({ title }) => {
  const images = [
    {
      url: av3,
      label: 'AV',
    },
    {
      url: av2,
      label: 'AV',
    },
    {
      label: 'AV',
    },
    {
      url: av1,
      label: 'AV',
    },
  ]
  return (
    <TopNavStyled>
      <div className="team-info">
        <div className="team-avatars">
          {/* {images.map((image, index) => (
            <Avatar
              key={index}
              className={`avatar ${index === 3 ? 'active' : ''}`}
              image={image}
            />
          ))}

          <Link to="/account-details" className="view-team">
            View Team
          </Link> */}
        </div>
        <div className="my-account">
          <div>
            <NotificationIcon
              height="20"
              width="18"
              className="notification-icon"
            />
          </div>

          <div className="profile">
            <Avatar className="avatar" image={images[3]} />
            <div className="d-flex flex-column">
              <span className="name">Tom Cook</span>
              <Link to={'/account-details'}>View Account</Link>
            </div>
          </div>
        </div>
      </div>
      <Breadcrumb title={title} />
    </TopNavStyled>
  )
}

TopNav.propTypes = {
  title: PropTypes.string,
}

import styled from 'styled-components'

export const BackgroundImageStyled = styled.div`
  .image-container {
    overflow: hidden;
    position: fixed;
    width: ${(props) => (props.width ? props.width : '50%')};
    height: 100%;
  }
`

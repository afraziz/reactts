import PropTypes from 'prop-types'
import { BackgroundImageStyled } from './styled'

export const BackgroundWrapper = ({ children, className, ...props }) => {
  return (
    <BackgroundImageStyled className={className} {...props}>
      <div className="image-container">{children}</div>
    </BackgroundImageStyled>
  )
}
BackgroundWrapper.propTypes = {
  children: PropTypes.any,
  className: PropTypes.any,
}

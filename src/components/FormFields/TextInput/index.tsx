import PropTypes from 'prop-types'
import { useField } from 'formik'
import styled, { css } from 'styled-components/macro'
import { Error } from '../../FormFields/Error'

const FormLabel = styled.label`
  font-size: ${(props) => props.theme.fontSizes.sm};
  color: ${(props) => props.theme.colors.hbTextGrey};
  font-weight: ${(props) => props.theme.fontWeights.semiBold};
  display: flex;
  flex-direction: row;
  margin-top: ${(props) =>
    props.labelMargin ? props.labelMargin : props.theme.space.sm};
  margin-bottom: ${(props) => props.theme.space.xxs2};
`

const TextInputStyles = css`
  background: white;
  border: 1px solid ${(props) => props.theme.colors.hbBorder};
  border-radius: 6px;
  padding: 1rem;
  width: ${(props) => (props.width ? props.width : '100%')};
  height: ${(props) => props.height && props.height};
  color: ${(props) => props.theme.colors.hbTextBlue};
  transition: ${(props) => props.theme.transitions.default};
  font-size: ${(props) => props.theme.fontSizes.md};
  -webkit-appearance: none;
  ::-webkit-input-placeholder {
    color: ${(props) => props.theme.colors.hbTextBlue};
    opacity: 0.5;
  }

  :disabled {
    background: #f9f9f9;
    border-color: #f9f9f9;
  }

  &:focus,
  &:hover {
    outline: none;
    border-color: ${(props) => props.theme.colors.hbPrimary};
    background: #fff;
  }
`

const TextInputStyled = styled.input`
  ${TextInputStyles}
`

export function TextInput(props) {
  const { label, className } = props
  const [field, meta] = useField(props)

  return (
    <div
      style={{
        position: 'relative',
        width: props.width ? props.width : 'auto',
        display: props.display ? props.display : 'block',
      }}
      className={className}
    >
      {label && (
        <FormLabel htmlFor={props.id || props.name} {...props}>
          {label}
        </FormLabel>
      )}
      <TextInputStyled {...field} {...props} />
      <div style={{ position: 'relative' }}>
        <Error meta={meta} />
      </div>
    </div>
  )
}

TextInput.propTypes = {
  className: PropTypes.any,
  display: PropTypes.any,
  id: PropTypes.any,
  label: PropTypes.any,
  name: PropTypes.any,
  width: PropTypes.any,
  type: PropTypes.any,
  labelMargin: PropTypes.any,
  required: PropTypes.any

}

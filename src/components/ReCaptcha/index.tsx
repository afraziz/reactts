import PropTypes from 'prop-types'
import ReCAPTCHA from 'react-google-recaptcha'

const siteKey =
  process.env.RECAPTCHA_SITE_KEY || '6Lf4E74eAAAAAF70W09HRTk9oMwwUi7kJHZDA6wn'

export const ReCathca = ({ recaptchaRef }) => {
  return (
    <ReCAPTCHA
      onChange={() => {}}
      ref={recaptchaRef}
      size="invisible"
      sitekey={siteKey}
    />
  )
}

ReCathca.propTypes = {
  recaptchaRef: PropTypes.any,
}

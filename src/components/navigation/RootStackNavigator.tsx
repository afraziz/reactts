import React, { Component } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import Intro from '../screen/Intro';
import NotFound from '../screen/NotFound';

class RootStackNavigator extends Component<any> {
  public render() {
    return (
      <BrowserRouter>
        <div style={{ textAlign: 'center' }}>
          <Routes>
            <Route  path='/' element={Intro} />
            <Route element={NotFound} />
          </Routes>
        </div>
      </BrowserRouter>
    );
  }
}

export default RootStackNavigator;

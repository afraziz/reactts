import styled from 'styled-components'

const FooterStyled = styled.div`
  padding-top: 32px;
  padding-bottom: 50px;
  margin-right: 34px;
  border-top: 1px solid ${(props) => props.theme.colors.hbFooterBorder};
  p {
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
  }
`

export const Footer = () => {
  return (
    <FooterStyled>
      <p>© 2022 XXXX. All rights reserved.</p>
    </FooterStyled>
  )
}

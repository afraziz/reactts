import PropTypes from 'prop-types'
import { HiHome } from 'react-icons/hi'
import { BreadcrumbIcon } from '../../images/svg'
import { BreadcrumbStyled } from './styled'

export const Breadcrumb = ({ title }) => {
  return (
    <BreadcrumbStyled>
      <HiHome className="home-icon" />
      <BreadcrumbIcon />
      <div className="page-title">{title}</div>
    </BreadcrumbStyled>
  )
}

Breadcrumb.propTypes = {
  title: PropTypes.string,
}

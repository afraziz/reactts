import { fetchUsers, userDeleted } from "../../features/user/usersSlice";
import { useDispatch, useSelector } from "react-redux";
import { Button, ButtonOutline } from 'components/Button'
import { InfoCard } from 'components/Cards/InfoCard'
import { Footer } from 'components/Footer'
import { Table } from 'components/Table'
import { TopNav } from 'components/TopNav'
import { SignoutIcon } from 'images/svg'
import { HeadingLarge } from 'styles/general-styles'
import { DashboardWrapper } from '../../layout/DashboardWrapper'
import { AccountDetailsStyled } from './styled'
import { Link, useNavigate } from "react-router-dom";
import { HiPlusCircle } from "react-icons/hi";
import { RootState } from "app/redux/RootReducer";

export function UserList() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { entities } = useSelector((state : RootState) => state.users);

  const handleDelete = (id) => {
    dispatch(userDeleted({ id }));
  };

  return (
    <AccountDetailsStyled>
    <DashboardWrapper>
      <TopNav />
      <div className="inner-content">
        <div className="d-flex justify-content-between">
          <HeadingLarge uppercase="true">Users List</HeadingLarge>
          <Button width="fit-content">
            <SignoutIcon className="mr-2" />
            {'  '}
            Sign Out
          </Button>
        </div>
        <InfoCard>
          <div className="card-header">
            <div>
              <h4>User Details</h4>
            </div>
            <Button width="fit-content" onClick={()=>navigate('/add-user')}>
                Add User <HiPlusCircle />
            </Button>
          </div>
          <div className="card-body">
            <Table className=" table-striped">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {entities.length &&
                entities.map(({ id, name, email }, i) => (
                  <tr key={i}>
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>{email}</td>
                    <td>
                        <div className="d-flex">
                            <ButtonOutline width="fit-content" onClick={() => handleDelete(id)}>Delete</ButtonOutline>
                            <Link to={`/edit-user/${id}`}>
                                <ButtonOutline width="fit-content">Edit</ButtonOutline>
                            </Link>
                        </div>
                    </td>
                  </tr>
                ))}
            </tbody>
            </Table>
          </div>
        </InfoCard>
      </div>
      <Footer />
    </DashboardWrapper>
  </AccountDetailsStyled>
  );
}

import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useLocation } from "react-router-dom";
import { Button} from 'components/Button'
import { InfoCard } from 'components/Cards/InfoCard'
import { Footer } from 'components/Footer'
import { TopNav } from 'components/TopNav'
import { SignoutIcon } from 'images/svg'
import { HeadingLarge } from 'styles/general-styles'
import { DashboardWrapper } from '../../layout/DashboardWrapper'
import { AccountDetailsStyled } from './styled'
import { useState } from "react";
import { userUpdated } from "../../features/user/usersSlice";
import { RootState } from "app/redux/RootReducer";

export function EditUser() {
  const { pathname } = useLocation();
  const userId = parseInt(pathname.replace("/edit-user/", ""));

  const user = useSelector((state: RootState) =>
    state.users.entities.find((user) => user.id === userId)
  );

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [name, setName] = useState(user.name);
  const [email, setEmail] = useState(user.email);
  const [error, setError] = useState(null);

  const handleName = (e) => setName(e.target.value);
  const handleEmail = (e) => setEmail(e.target.value);

  const handleClick = () => {
    if (name && email) {
      dispatch(
        userUpdated({
          id: userId,
          name,
          email,
        })
      );

      setError(null);
      navigate("/user-list");
    } else {
      setError("Fill in all fields");
    }
  };

  return (
    <AccountDetailsStyled>
    <DashboardWrapper>
      <TopNav />
      <div className="inner-content">
        <div className="d-flex justify-content-between">
          <HeadingLarge uppercase="true">Users List</HeadingLarge>
          <Button width="fit-content">
            <SignoutIcon className="mr-2" />
            {'  '}
            Sign Out
          </Button>
        </div>
        <InfoCard>
          <div className="card-header">
            <div>
              <h4>Edit User Details</h4>
            </div>
          </div>
          <div className="form card-body">
                <div className="three columns">
                <label htmlFor="nameInput">Name</label>
                <div className="row">
                    <div className="col-sm-12">
                        <input
                            className="u-full-width"
                            type="text"
                            placeholder="test@mailbox.com"
                            id="nameInput"
                            onChange={handleName}
                            value={name}
                            />
                    </div>
                </div>
                <label htmlFor="emailInput">Email</label>
                <div className="row">
                    <div className="col-sm-12">
                        <input
                            className="u-full-width"
                            type="email"
                            placeholder="test@mailbox.com"
                            id="emailInput"
                            onChange={handleEmail}
                            value={email}
                        />
                    </div>
                </div>
              
                {error && error}
                <Button width="fit-content" onClick={handleClick} >
                    Save user
                </Button>
                </div>
          </div>
        </InfoCard>
      </div>
      <Footer />
    </DashboardWrapper>
  </AccountDetailsStyled>
  );
}
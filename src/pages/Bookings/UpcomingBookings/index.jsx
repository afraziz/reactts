import PropTypes from 'prop-types'
import { Row, Col } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'
import { connect } from 'react-redux'

import { fullResetBooking } from 'features/booking/bookingSlice'
// import { Submit } from 'components/Common/Submit';
import {
  ContentWrapper,
  HeadingMedium,
  NoPaddingContainer,
} from 'styles/general-styles'
// import { UpcomingBookingsCol } from 'styles/bookings-styles';
import { Submit } from 'components/FormFields/Submit'

const UpcomingBookings = ({ fullResetBooking }) => {
  const navigate = useNavigate()

  // const DATA = [
  //   {
  //     id: '1',
  //     type: 'Incomplete',
  //     image: require('images/upcomingBookings/upcoming_high_res.jpg'),
  //   },
  //   {
  //     id: '2',
  //     type: 'Pending',
  //     image: require('images/upcomingBookings/upcoming_high_res.jpg'),
  //   },
  //   {
  //     id: '3',
  //     type: 'Incomplete',
  //     image: require('images/upcomingBookings/upcoming_high_res.jpg'),
  //   },
  // ]

  return (
    <ContentWrapper>
      <NoPaddingContainer fluid>
        <Row>
          <Col>
            <HeadingMedium style={{ marginBottom: 30 }}>Upcoming</HeadingMedium>
          </Col>
        </Row>
        <Row>
          {
            // DATA.map((d, i) =>
            //     <UpcomingBookingsCol md={12} key={i} marginbottom={true}>
            //         <Link to="/bookings/upcoming/detail" state={{ bookingId: d.id }}>
            //             <div className="content">
            //                 <label className="type">{d.type}</label>
            //             </div>
            //             <img src={d.image.default} alt="upcoming bookings" />
            //         </Link>
            //     </UpcomingBookingsCol>
            // )
          }
        </Row>
      </NoPaddingContainer>

      <Submit
        type="button"
        marginTop="100px"
        text="CREATE A NEW BOOKING"
        onClick={() => {
          fullResetBooking()
          navigate('/bookings/new')
        }}
      />
    </ContentWrapper>
  )
}

UpcomingBookings.propTypes = {
  fullResetBooking: PropTypes.func,
}

export default connect(null, { fullResetBooking })(UpcomingBookings)

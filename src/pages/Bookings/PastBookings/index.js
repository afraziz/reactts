import PropTypes from 'prop-types'
import { Col, Row } from 'react-bootstrap'
import { connect } from 'react-redux'

import { fullResetBooking } from 'features/booking/bookingSlice'
import {
  HeadingMedium,
  ContentWrapper,
  NoPaddingContainer,
} from 'styles/general-styles'
// import { PastBookingsCol } from 'styles/bookings-styles'
// import { Submit } from 'components/Common/Submit'

const PastBookings = () => {
  // const DATA = [
  // {
  //   id: '1',
  //   date: '25 Mar 2020',
  //   cattleSent: 100,
  //   image: require('images/pastBookings/past-bookings1.png'),
  // },
  // {
  //   id: '2',
  //   date: '19 Dec 2019',
  //   cattleSent: 120,
  //   image: require('images/pastBookings/past-bookings2.png'),
  // },
  // {
  //   id: '3',
  //   date: '25 Mar 2020',
  //   cattleSent: 100,
  //   image: require('images/pastBookings/past-bookings1.png'),
  // },
  // {
  //   id: '4',
  //   date: '19 Dec 2019',
  //   cattleSent: 120,
  //   image: require('images/pastBookings/past-bookings2.png'),
  // },
  // {
  //   id: '5',
  //   date: '25 Mar 2020',
  //   cattleSent: 100,
  //   image: require('images/pastBookings/past-bookings1.png'),
  // },
  // {
  //   id: '6',
  //   date: '19 Dec 2019',
  //   cattleSent: 120,
  //   image: require('images/pastBookings/past-bookings2.png'),
  // },
  // ]

  return (
    <ContentWrapper>
      <NoPaddingContainer fluid>
        <Row>
          <Col>
            <HeadingMedium style={{ marginBottom: 30 }}>
              Past bookings
            </HeadingMedium>
          </Col>
        </Row>
        <Row>
          {/* {DATA.map((d, i) => (
            <PastBookingsCol md={4} key={i}>
              <div className="content">
                <label className="date">{d.date}</label>
                <label className="cattle-sent">
                  {d.cattleSent} Cattle sent
                </label>
              </div>
              <img src={d.image.default} alt="past bookings" />
            </PastBookingsCol>
          ))} */}
        </Row>
      </NoPaddingContainer>

      {/* <Submit
        type="submit"
        marginTop="100px"
        text="CREATE A NEW BOOKING"
        onClick={() => {
          fullResetBooking()
          navigate('/bookings/new')
        }}
      /> */}
    </ContentWrapper>
  )
}

PastBookings.propTypes = {
  fullResetBooking: PropTypes.func,
}

export default connect(null, { fullResetBooking })(PastBookings)

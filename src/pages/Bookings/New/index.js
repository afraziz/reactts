import PropTypes from 'prop-types'
import { useEffect, useState } from 'react'
import { connect } from 'react-redux'

import { setProgress } from 'features/registration/registrationSlice'
// import AddTotalAmount from 'components/Bookings/AddTotalAmount'
// import AddCattle from 'components/Bookings/AddCattle'
// import CattleDetails from 'components/Bookings/CattleDetails'
// import EnterAge from 'components/Bookings/EnterAge'
// import LiveWeight from 'components/Bookings/LiveWeight'
// import AddMedia from 'components/Bookings/AddMedia'
// import AdditionalInfo from 'components/Bookings/AdditionalInfo'
// import DeliveryDate from 'components/Bookings/DeliveryDate'
// import AltDeliveryDate from 'components/Bookings/AltDeliveryDate'
// import Breed from 'components/Bookings/Breed'
// import ReviewBooking from 'components/Bookings/ReviewBooking'
// import BookingSubmitted from 'components/Bookings/BookingSubmitted'
// import AgentAccess from 'components/Bookings/AgentAccess'
// import ProgressSteps from 'components/Bookings/ProgressSteps'

const New = ({ bookings }) => {
  const [currentStep] = useState()
  // const [percent, setPercent] = useState(0);

  useEffect(() => {
    // switch (bookings.progressStep) {
    //   case 1:
    //     setCurrentStep(<AddTotalAmount steps={<ProgressSteps percent={0} />} />)
    //     break
    //   case 2:
    //     setCurrentStep(<AddCattle steps={<ProgressSteps percent={16} />} />)
    //     break
    //   case 3:
    //     setCurrentStep(<CattleDetails />)
    //     break
    //   case 4:
    //     setCurrentStep(<EnterAge />)
    //     break
    //   case 5:
    //     setCurrentStep(<LiveWeight />)
    //     break
    //   case 6:
    //     setCurrentStep(<AddMedia />)
    //     break
    //   case 7:
    //     setCurrentStep(<AdditionalInfo />)
    //     break
    //   case 8:
    //     setCurrentStep(<Breed />)
    //     break
    //   case 9:
    //     setCurrentStep(<DeliveryDate steps={<ProgressSteps percent={32} />} />)
    //     break
    //   case 10:
    //     setCurrentStep(
    //       <AltDeliveryDate steps={<ProgressSteps percent={48} />} />
    //     )
    //     break
    //   case 11:
    //     setCurrentStep(<ReviewBooking steps={<ProgressSteps percent={64} />} />)
    //     break
    //   case 12:
    //     setCurrentStep(<BookingSubmitted />)
    //     break
    //   case 13:
    //     setCurrentStep(<AgentAccess />)
    //     break
    //   default:
    //     setCurrentStep(<AddTotalAmount />)
    //     break
    // }
  }, [bookings.progressStep])

  return <>{currentStep}</>
}

New.propTypes = {
  bookings: PropTypes.shape({
    progressStep: PropTypes.any,
  }),
  setProgress: PropTypes.any,
}

const mapStateToProps = (state) => ({
  bookings: state.bookings,
})

export default connect(mapStateToProps, { setProgress })(New)

// export default New;

import { useState } from 'react'
import PropTypes from 'prop-types'
import { HeadingLarge } from 'styles/general-styles'
import { FloatingHelpIcon } from 'components/FloatingHelpIcon'
import { ResetPasswordForm } from 'components/Forms/ForgotPassword'
import { RegisterNavigation } from 'components/Navigation'
import { HelpModal } from 'components/Modals/HelpModal'
import { BackgroundWrapper } from 'components/BackgroundWrapper'
import { ResetPasswordStyled } from './styled'

const img = require('images/reset-bg.jpeg')

export const ResetPasswordPage = ({ handleNavigation }) => {
  const [isModalOpen, setModalIsOpen] = useState(false)

  return (
    <ResetPasswordStyled>
      <BackgroundWrapper className="bg-image" width="100%">
        <img
          src={img}
          alt="background"
          style={{
            width: '100%',
            height: '100%',
            objectFit: 'cover',
            zIndex: 0,
          }}
        />
      </BackgroundWrapper>

      <RegisterNavigation
        className="navigation"
        title={'Reset your password'}
        backUrl="/login"
      />
      <div className="container">
        <div className="row d-flex justify-content-center">
          <div className="col-lg-7 col-md-9 col-12">
            <div className="form-card">
              <div className="header-text">
                <HeadingLarge>Reset your password</HeadingLarge>
                <p>
                  Please enter and confirm your new password. Your password must
                  be at least 8 characters.
                </p>
              </div>
              <ResetPasswordForm handleNavigation={handleNavigation} />
            </div>
          </div>
        </div>
      </div>
      <HelpModal isOpen={isModalOpen} setModalIsOpen={setModalIsOpen} />
      <FloatingHelpIcon
        isModalOpen={isModalOpen}
        setModalIsOpen={setModalIsOpen}
      />
    </ResetPasswordStyled>
  )
}
ResetPasswordPage.propTypes = {
  handleNavigation: PropTypes.any,
}

import styled from 'styled-components'

export const ResetPasswordStyled = styled.div`
  .bg-image {
    display: none;
    ${(props) => props.theme.mediaQueries.md} {
      display: block;
    }
  }
  .navigation {
    display: block;
    ${(props) => props.theme.mediaQueries.md} {
      display: none;
    }
  }
  .form-card {
    margin-bottom: 83px;
    margin-top: 32px;
    padding: 0;
    background: ${(props) => props.theme.colors.white};
    border-radius: 8px;
    .back-to-login {
      color: ${(props) => props.theme.colors.hbTextGreyLight};
      position: relative;

      display: none;
      svg {
        position: absolute;
        left: -25px;
        top: 0px;
        font-size: 20px;
      }
      &:hover {
        text-decoration: none;
      }
      ${(props) => props.theme.mediaQueries.md} {
        display: block;
      }
    }
    .logo {
      height: 68px;
      img {
        height: 100%;
        width: auto;
      }
    }
    .header-text {
      display: flex;
      flex-direction: column;
      text-align: center;
      padding: 0 6px;
      h1 {
        margin-top: 24px;
        margin-bottom: 8px;
        text-transform: uppercase;
      }
      p {
        font-size: 14px;
        font-weight: 500;
        color: ${(props) => props.theme.colors.hbTextGreyLight};
        line-height: 20px;
      }
      ${(props) => props.theme.mediaQueries.sm} {
        padding: 0 10px;
      }
      ${(props) => props.theme.mediaQueries.md} {
        padding: 0 20px;
      }
      ${(props) => props.theme.mediaQueries.lg} {
        padding: 0 77px;
      }
    }
    ${(props) => props.theme.mediaQueries.md} {
      padding: 30px 48px 48px 48px;
      margin-top: 25vh;
    }
  }
`
export const PasswordResetSuccessStyled = styled.div`
  .bg-image {
    display: none;
    ${(props) => props.theme.mediaQueries.md} {
      display: block;
    }
  }
  .navigation {
    display: block;
    ${(props) => props.theme.mediaQueries.md} {
      display: none;
    }
  }
  .info-card {
    margin-top: 40px;
    margin-bottom: 186px;
    background: ${(props) => props.theme.colors.white};
    padding: 5px;
    border-radius: 8px;
    text-align: center;
    .success-logo {
      display: flex;
      justify-content: start;
      .call-icon {
        margin-bottom: 20px;
        padding: 10px;
        font-size: 44px;
        border-radius: 50%;
        width: 76px;
        height: 76px;
        background: ${(props) => props.theme.colors.hbSuccess};
        color: ${(props) => props.theme.colors.hbSuccessText};
        display: flex;
        justify-content: center;
        align-items: center;
      }
    }
    h1 {
      margin-bottom: 16px;
      text-align: left;
    }
    p {
      margin-bottom: 24px;
      text-align: left;
    }
    ${(props) => props.theme.mediaQueries.sm} {
      padding: 20px;
      margin-top: 90px;
      .success-logo {
        justify-content: center;
      }
      h1 {
        margin-bottom: 20px;
        text-align: center;
      }
      p {
        text-align: center;
      }
    }
    ${(props) => props.theme.mediaQueries.md} {
      padding: 40px;
      margin-top: 186px;
    }
  }
`

import PropTypes from 'prop-types'
import { BackgroundWrapper } from 'components/BackgroundWrapper'
import { Progressbar } from 'components/Progressbar'
import {
  SignupStepBaseStyled,
  RegistrationFormWrapper,
} from '@page/Auth/Signup/styled'
import VerifyNumberForm from 'components/Forms/Registration/VerifyNumberForm'
import { connect } from 'react-redux'
import { currentProgress } from 'features/utils/helper'

const backgroundImage = require('images/got-a-code-screen.png')

const VerifyMobileNumber = ({ totalSteps, progressStep, navigation }) => {
  const progress = currentProgress(totalSteps, progressStep)

  return (
    <SignupStepBaseStyled>
      <BackgroundWrapper className="image-section">
        <img
          src={backgroundImage}
          alt="background"
          style={{
            width: '100%',
            height: '100%',
            objectFit: 'cover',
            zIndex: 0,
          }}
        />
      </BackgroundWrapper>

      <div className="form-section">
        {navigation('Verify Mobile Number')}
        <Progressbar progress={progress} />
        <RegistrationFormWrapper>
          <VerifyNumberForm />
        </RegistrationFormWrapper>
      </div>
    </SignupStepBaseStyled>
  )
}

VerifyMobileNumber.propTypes = {
  navigation: PropTypes.func,
  progressStep: PropTypes.any,
  totalSteps: PropTypes.any,
}
const mapStateToProps = (state) => ({
  progressStep: state.registration.progressStep,
  totalSteps: state.registration.totalSteps,
})
export default connect(mapStateToProps, {})(VerifyMobileNumber)

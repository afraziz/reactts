import styled from 'styled-components'

export const AccountCreatedStyled = styled.div`
  .bg-image {
    display: none;
    ${(props) => props.theme.mediaQueries.md} {
      display: block;
    }
  }
  .navigation {
    display: block;
    ${(props) => props.theme.mediaQueries.md} {
      display: none;
    }
  }
  .info-card {
    margin-top: 40px;
    margin-bottom: 186px;
    background: ${(props) => props.theme.colors.white};
    padding: 5px;
    border-radius: 8px;
    text-align: center;
    .success-logo {
      display: flex;
      justify-content: start;
      .call-icon {
        margin-bottom: 20px;
        padding: 10px;
        font-size: 44px;
        border-radius: 50%;
        width: 76px;
        height: 76px;
        background: ${(props) => props.theme.colors.hbSuccess};
        color: ${(props) => props.theme.colors.hbSuccessText};
        display: flex;
        justify-content: center;
        align-items: center;
      }
    }
    h1 {
      margin-bottom: 16px;
      text-align: left;
    }
    p {
      margin-bottom: 24px;
      text-align: left;
    }
    ${(props) => props.theme.mediaQueries.sm} {
      padding: 20px;
      margin-top: 90px;
      .success-logo {
        justify-content: center;
      }
      h1 {
        margin-bottom: 20px;
        text-align: center;
      }
      p {
        text-align: center;
      }
    }
    ${(props) => props.theme.mediaQueries.md} {
      padding: 40px;
      margin-top: 186px;
    }
  }
`

import PropTypes from 'prop-types'
import { useState } from 'react'
import {
  SignupStepBaseStyled,
  RegistrationFormWrapper,
} from '@page/Auth/Signup/styled'
import { HelpModal } from 'components/Modals'
import { BackgroundWrapper } from 'components/BackgroundWrapper'
import ContactDetailsForm from 'components/Forms/Registration/ContactDetailsForm'
import { Progressbar } from 'components/Progressbar'
import { connect } from 'react-redux'
import { currentProgress } from 'features/utils/helper'

const backgroundImage = require('images/welcome-image.png')

const EmailComponent = ({ totalSteps, progressStep, navigation }) => {
  const [isModalOpen, setModalIsOpen] = useState(false)

  const progress = currentProgress(totalSteps, progressStep)

  return (
    <SignupStepBaseStyled>
      <BackgroundWrapper className="image-section">
        <img
          src={backgroundImage}
          alt="background"
          style={{
            width: '100%',
            height: '100%',
            objectFit: 'cover',
            zIndex: 0,
          }}
        />
      </BackgroundWrapper>

      <div className="form-section">
        {navigation('Verify Mobile Number')}
        <Progressbar progress={progress} />
        <RegistrationFormWrapper>
          <ContactDetailsForm />
        </RegistrationFormWrapper>
      </div>
      <HelpModal isOpen={isModalOpen} setModalIsOpen={setModalIsOpen} />
    </SignupStepBaseStyled>
  )
}

EmailComponent.propTypes = {
  navigation: PropTypes.func,
  progressStep: PropTypes.any,
  totalSteps: PropTypes.any,
}
const mapStateToProps = (state) => ({
  progressStep: state.registration.progressStep,
  totalSteps: state.registration.totalSteps,
})

export const Email = connect(mapStateToProps, {})(EmailComponent)

import styled from 'styled-components'

export const SignupStyled = styled.div``

export const SignupStepBaseStyled = styled.div`
  display: flex;
  background: ${(props) => props.theme.colors.white};
  .image-section {
    flex: 1;
    ${(props) => props.theme.mediaQueries.md} {
      display: block;
    }
    display: none;
  }
  .form-section {
    flex: 1;
    height: 100%;
    background: #fff;
    z-index: 1;
    min-height: 100vh;
  }
`

export const RegistrationFormWrapper = styled.div`
  width: 100%;
  padding: 16px;
  min-height: 100%;
  margin: auto;
  margin-top: 38px;
  margin-bottom: 65px;
  h4 {
    text-transform: uppercase;
    font-weight: ${(props) => props.theme.fontWeights.bold};
    font-size: ${(props) => props.theme.fontSizes.md_2};
    background: ${(props) => props.theme.colors.hbGreyBackground};
    color: ${(props) => props.theme.colors.hbTextBlue};
    letter-spacing: 0.01em;
    width: fit-content;
    padding: 0 3px;
    margin-bottom: 20px;
  }
  h1 {
    margin-bottom: 16px;
  }
  p {
    margin-bottom: 26px;
  }
  .meter-input-container {
    margin-bottom: 9px;
  }
  .signup {
    color: ${(props) => props.theme.colors.hbTextBlue};
    text-align: left;
    margin-top: 26px;
  }
  .receive-your-code {
    color: ${(props) => props.theme.colors.hbTextGreyLight};
    text-align: left;
    margin-top: 26px;
  }
  .click-here-link {
    color: ${(props) => props.theme.colors.hbPrimary};
  }
  .click-here-link:hover {
    color: ${(props) => props.theme.colors.hbPrimaryHover};
  }
  ${(props) => props.theme.mediaQueries.sm} {
    width: 440px;
    margin-top: 65px;
  }
  ${(props) => props.theme.mediaQueries.lg} {
    width: 440px;
    margin-top: 65px;
  }
`

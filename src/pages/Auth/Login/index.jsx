import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { HeadingLarge } from 'styles/general-styles'
import { BackgroundWrapper } from '../../../components/BackgroundWrapper'
import { signin } from '../../../features/auth/authSlice'
import { LoginForm } from '../../../components/Forms/Login'
import { useNavigate } from 'react-router-dom'
import { useEffect, useState } from 'react'
import { RegisterNavigation } from '../../../components/Navigation'
import { HelpModal } from '../../../components/Modals'
import { LoginStyled } from './styled'

const bgImg = require('images/login_bg.jpg')
const logo = require('images/react_logo.png')

const Login = ({ auth, signin }) => {
  const navigate = useNavigate()
  const token = localStorage.getItem('is_logged_in') === 'true'
  const [isModalOpen, setModalIsOpen] = useState(false)

  useEffect(() => {
    // token && signin()
  }, [signin, token])

  if (auth.isLoggedIn || token) {
    navigate('account/details')
  }

  return (
    <LoginStyled>
      <BackgroundWrapper className="bg-image" width="100%">
        <img
          src={bgImg}
          alt="background"
          style={{
            width: '100%',
            height: '100%',
            objectFit: 'cover',
            zIndex: 0,
          }}
        />
      </BackgroundWrapper>
      <RegisterNavigation
        title={'Sign in'}
        hideBack
        isModalOpen={isModalOpen}
        setModalIsOpen={setModalIsOpen}
        className="navigation"
      />
      <div className="container">
        <div className="row d-flex justify-content-center">
          <div className="col-lg-7 col-md-9 col-12">
            <div className="form-card">
              <div className="header-text">
                <div className="logo">
                  <img src={logo} alt="background" />
                </div>
                <HeadingLarge>Sign in to your ACCOUNT</HeadingLarge>
                <p>
                React is a free and open-source front-end JavaScript library for building user interfaces based on UI components.
                </p>
              </div>
              <LoginForm signin={signin} />
            </div>
          </div>
        </div>
      </div>
      <HelpModal isOpen={isModalOpen} setModalIsOpen={setModalIsOpen} />
    </LoginStyled>
  )
}

Login.propTypes = {
  auth: PropTypes.shape({
    isLoggedIn: PropTypes.any,
  }),
  signin: PropTypes.func,
}
const mapStateToProps = (state) => ({
  auth: state.auth,
})
export default connect(mapStateToProps, { signin })(Login)

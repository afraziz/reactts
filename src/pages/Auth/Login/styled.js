import styled from 'styled-components'

export const LoginStyled = styled.div`
  .bg-image {
    display: none;
    ${(props) => props.theme.mediaQueries.md} {
      display: block;
    }
  }
  .navigation {
    display: block;
    ${(props) => props.theme.mediaQueries.md} {
      display: none;
    }
  }
  .form-card {
    margin-top: 48px;
    margin-bottom: 83px;
    padding: 0;
    background: ${(props) => props.theme.colors.white};
    border-radius: 8px;
    .logo {
      height: 68px;
      img {
        height: 100%;
        width: auto;
      }
    }
    .header-text {
      display: flex;
      flex-direction: column;
      text-align: center;
      padding: 0 6px;
      h1 {
        margin-top: 24px;
        margin-bottom: 8px;
        text-transform: uppercase;
      }
      p {
        font-size: 14px;
        font-weight: 500;
        color: ${(props) => props.theme.colors.hbTextGreyLight};
        line-height: 20px;
      }
      ${(props) => props.theme.mediaQueries.sm} {
        padding: 0 10px;
      }
      ${(props) => props.theme.mediaQueries.md} {
        padding: 0 20px;
      }
      ${(props) => props.theme.mediaQueries.lg} {
        padding: 0 77px;
      }
    }
    a {
      font-weight: 500;
      font-size: 14px;
      text-decoration: none;
      color: ${(props) => props.theme.colors.hbPrimary};
    }
    a:hover {
      color: ${(props) => props.theme.colors.hbPrimaryHover};
    }
    .signup {
      color: ${(props) => props.theme.colors.hbTextBlue};
      text-align: center;
      margin-top: 24px;
      display: flex;
      flex-direction: column;
      ${(props) => props.theme.mediaQueries.md} {
        flex-direction: row;
        justify-content: center;
      }
    }
    ${(props) => props.theme.mediaQueries.md} {
      padding: 48px;
      margin-top: 101px;
    }
  }
`

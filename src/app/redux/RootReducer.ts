import { combineReducers } from 'redux'
import regReducer from '../../features/registration/registrationSlice'
import usersReducer from '../../features/user/usersSlice'
import authReducer from '../../features/auth/authSlice'

export const rootReducer = combineReducers({
  auth: authReducer,
  registration: regReducer,
  users: usersReducer,
})
export type RootState = ReturnType<typeof rootReducer>

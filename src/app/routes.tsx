import { Routes as ReactRoutes, Route, Navigate } from 'react-router-dom'
import Bookings from '../pages/Bookings'
import UpcomingBookings from '../pages/Bookings/UpcomingBookings'
import { ForgotPassword } from '../pages/Auth/Password/ForgotPassword'
import { ResetPasswordIndex } from '../pages/Auth/Password/ResetPassword'
import { AddUser } from '../pages/Users/AddUser'
import { UserList } from '../pages/Users/UserList'
import { Team } from '../pages/Accounts/Team'
import Signup from '../pages/Auth/Signup'
import Login from '../pages/Auth/Login'
import RequireAuth from './RequireAuth'
import { EditUser } from '../pages/Users/EditUser'
import AccountDetails from '../pages/Accounts/AccountDetails'
function Routes() {
  return (
    <ReactRoutes>
      <Route path="/" element={<Navigate replace to="/account-details" />} />
      <Route path="/forgot-password" element={<ForgotPassword />} />
      <Route path="/add-user" element={<AddUser />} />
      <Route path="/user-list" element={<UserList />} />
      <Route path="/edit-user/*" element={<EditUser />} />
      <Route path="/account-details" element={<AccountDetails />} />
      <Route path="/team" element={<Team />} />
      <Route
        path="/signup/"
        element={<Navigate replace to="/signup/supplier" />}
      />
      <Route path="signup/:type" element={<Signup />} />
      <Route path="/login" element={<Login />} />

      <Route path="bookings" element={<RequireAuth />}>
        {/* <Route path="bookings" > */}
        <Route
          index
          element={<Navigate replace={true} to="/bookings/upcoming" />}
        />

        <Route path="upcoming" element={<Bookings />}>
          <Route index element={<UpcomingBookings />} />
        </Route>

        {/* <Route path="past" element={<Bookings />}>
          <Route index element={<PastBookings />} />
        </Route> */}

        {/* <Route path="upcoming/detail" element={<UpcomingBookingDetail />} />
        <Route path="new" element={<NewBookings />} /> */}
      </Route>
    </ReactRoutes>
  )
}

export default Routes

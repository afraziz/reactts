import { createSlice } from '@reduxjs/toolkit'

export const BookingsTypes = {
  AGE: '',
  AVG_LIVE_WEIGHT: '',
  LOWEST_WEIGHT: '',
  HIGHEST_WEIGHT: '',
  BREED: '',
  ADDITIONAL_INFO: '',
  FEMALE_QUANTITY: '',
  MALE_QUANTITY: '',
  TOTAL_CATTLE: '',
  TOTAL_CATTLE_ADDED: '',
  CATTLE: [],
  DELIVERY_DATE: '',
  ALT_DELIVERY_DATE: '',
  VISITED_STEPS: [],
  MEDIA: [],
  AGENT_ACCESS: {},
  CATTLE_TYPE: '',
  PROGRESS_STEP: 1,
  EDIT_ID: null,
}

const bookingSlice = createSlice({
  name: 'booking',
  initialState: {
    age: BookingsTypes.AGE,
    avgLiveWeight: BookingsTypes.AVG_LIVE_WEIGHT,
    lowestWeight: BookingsTypes.LOWEST_WEIGHT,
    highestWeight: BookingsTypes.HIGHEST_WEIGHT,
    breed: BookingsTypes.BREED,
    additionalInfo: BookingsTypes.ADDITIONAL_INFO,
    maleQuantity: BookingsTypes.FEMALE_QUANTITY,
    femaleQuantity: BookingsTypes.MALE_QUANTITY,
    totalCattle: BookingsTypes.TOTAL_CATTLE,
    totalCattleAdded: BookingsTypes.TOTAL_CATTLE_ADDED,
    cattle: BookingsTypes.CATTLE,
    deliveryDate: BookingsTypes.DELIVERY_DATE,
    altDeliveryDate: BookingsTypes.ALT_DELIVERY_DATE,
    visitedSteps: BookingsTypes.VISITED_STEPS,
    media: BookingsTypes.MEDIA,
    agentAccess: BookingsTypes.AGENT_ACCESS,
    cattleType: BookingsTypes.CATTLE_TYPE,
    progressStep: BookingsTypes.PROGRESS_STEP,
    editId: BookingsTypes.EDIT_ID,
  },
  reducers: {
    resetBooking(state) {
      // for current booking
      return {
        ...state,
        age: BookingsTypes.AGE,
        breed: BookingsTypes.BREED,
        avgLiveWeight: BookingsTypes.AVG_LIVE_WEIGHT,
        lowestWeight: BookingsTypes.LOWEST_WEIGHT,
        highestWeight: BookingsTypes.HIGHEST_WEIGHT,
        additionalInfo: BookingsTypes.ADDITIONAL_INFO,
        maleQuantity: BookingsTypes.FEMALE_QUANTITY,
        femaleQuantity: BookingsTypes.MALE_QUANTITY,
        media: BookingsTypes.MEDIA,
      }
    },
    fullResetBooking(state) {
      // all bookings
      return {
        ...state,
        age: BookingsTypes.AGE,
        breed: BookingsTypes.BREED,
        avgLiveWeight: BookingsTypes.AVG_LIVE_WEIGHT,
        lowestWeight: BookingsTypes.LOWEST_WEIGHT,
        highestWeight: BookingsTypes.HIGHEST_WEIGHT,
        additionalInfo: BookingsTypes.ADDITIONAL_INFO,
        maleQuantity: BookingsTypes.FEMALE_QUANTITY,
        femaleQuantity: BookingsTypes.MALE_QUANTITY,
        deliveryDate: BookingsTypes.DELIVERY_DATE,
        altDeliveryDate: BookingsTypes.ALT_DELIVERY_DATE,
        media: BookingsTypes.MEDIA,
        agentAccess: BookingsTypes.AGENT_ACCESS,
        totalCattle: BookingsTypes.TOTAL_CATTLE,
        cattle: BookingsTypes.CATTLE,
        progressStep: BookingsTypes.PROGRESS_STEP,
      }
    },
    setMaleQuantity(state, action) {
      return {
        ...state,
        maleQuantity: action.payload,
      }
    },
    setFemaleQuantity(state, action) {
      return {
        ...state,
        femaleQuantity: action.payload,
      }
    },
    setAge(state, action) {
      return {
        ...state,
        age: action.payload,
      }
    },
    setWeight(state, action) {
      return {
        ...state,
        avgLiveWeight: action.payload.avgLiveWeight,
        lowestWeight: action.payload.lowestWeight,
        highestWeight: action.payload.highestWeight,
      }
    },
    setBreed(state, action) {
      return {
        ...state,
        breed: action.payload,
      }
    },
    setAdditionalInfo(state, action) {
      return {
        ...state,
        additionalInfo: action.payload,
      }
    },
    setTotalCattle(state, action) {
      return {
        ...state,
        totalCattle: action.payload,
      }
    },
    setCattleType(state, action) {
      return {
        ...state,
        cattleType: action.payload,
      }
    },
    addCattleDetails(state, action) {
      return {
        ...state,
        cattleDetails: action.payload,
      }
    },
    setProgress(state, action) {
      return {
        ...state,
        progressStep: action.payload.progressStep,
      }
    },
    setQuantity(state, action) {
      return {
        ...state,
        maleQuantity: action.payload.maleQuantity,
        femaleQuantity: action.payload.femaleQuantity,
      }
    },
    setCattle(state, action) {
      let arrIndex = 0
      const editedCattle = state.cattle.find((c, i) => {
        arrIndex = i
        return c.id === action.payload.id
      })
      let cattleToBeSaved = []

      if (editedCattle) {
        cattleToBeSaved = state.cattle.map((item, index) => {
          if (index === arrIndex) {
            return action.payload
          }

          // Leave every other item unchanged
          return item
        })
      } else {
        cattleToBeSaved = state.cattle.concat(action.payload)
      }

      return {
        ...state,
        cattle: cattleToBeSaved,
      }
    },
    editCattle(state, action) {
      return {
        ...state,
        id: action.payload.id,
        age: action.payload.age,
        breed: action.payload.breed,
        avgLiveWeight: action.payload.avgLiveWeight,
        lowestWeight: action.payload.lowestWeight,
        highestWeight: action.payload.highestWeight,
        additionalInfo: action.payload.additionalInfo,
        maleQuantity: action.payload.maleQuantity,
        femaleQuantity: action.payload.femaleQuantity,
        media: action.payload.media,
      }
    },
    setTotalCattleAdded(state, action) {
      return {
        ...state,
        totalCattleAdded: action.payload,
      }
    },
    setDeliveryDate(state, action) {
      return {
        ...state,
        deliveryDate: action.payload,
      }
    },
    setAltDeliveryDate(state, action) {
      return {
        ...state,
        altDeliveryDate: action.payload,
      }
    },
    setEditId(state, action) {
      return {
        ...state,
        editId: action.payload,
      }
    },
    setMedia(state, action) {
      return {
        ...state,
        media: action.payload,
      }
    },
    removeMedia(state, action) {
      const mediaArr = state.media.filter((item, index) => {
        // Remove item "X"
        // alternatively: you could look for a specific index
        if (index === action.payload) {
          return false
        }

        // Every other item stays
        return true
      })

      return {
        ...state,
        media: mediaArr,
      }
    },
    setAgentAccess(state, action) {
      return {
        ...state,
        agentAccess: action.payload,
      }
    },
  },
})

export const {
  setProgress,
  setTotalCattle,
  setCattleType,
  addCattleDetails,
  resetBooking,
  setAge,
  setWeight,
  setAdditionalInfo,
  setBreed,
  setQuantity,
  setCattle,
  setTotalCattleAdded,
  setDeliveryDate,
  setAltDeliveryDate,
  fullResetBooking,
  editCattle,
  setEditId,
  setMaleQuantity,
  setFemaleQuantity,
  setMedia,
  removeMedia,
  setAgentAccess,
} = bookingSlice.actions

export default bookingSlice.reducer

import axios from 'axios'
import store from '../../app/redux/store'

import {
  setProgress,
  setSupplierId,
  saveBusinessDetails,
  saveAccessToken,
  setIsExistingSupplier,
} from './registrationSlice'

const { REACT_APP_API_URL } = process.env

axios.defaults.withCredentials = true
function listener() {
  const token = store.getState().registration?.accessToken
  axios.defaults.headers.common = { Authorization: `Bearer ${token}` }
}

store.subscribe(listener)

export const verifySupplierId = (values) => {
  return async (dispatch) => {
    let result = null

    await axios
      .post(`${REACT_APP_API_URL}/supplier/verify`, values)
      .then((res) => {
        if (res.data.success === true) {
          dispatch({
            type: setIsExistingSupplier,
            payload: true,
          })
          dispatch({
            type: setSupplierId,
            payload: values.supplier_code,
          })
        }
        result = res
      })
      .catch((e) => {
        const error = { ...e }
        result = error.response.data
      })
    return result
  }
}

export const register = (values) => {
  return async (dispatch) => {
    let result = null

    await axios
      .post(`${REACT_APP_API_URL}/users`, values)
      .then((res) => {
        if (res.data.success === true) {
          dispatch({ type: saveBusinessDetails, payload: res.data.data.user })
          if (res.data.meta?.access_token) {
            dispatch({
              type: saveAccessToken,
              payload: res.data.meta.access_token,
            })
          }
        }
        result = res.data
      })
      .catch((e) => {
        const error = { ...e }
        result = error.response.data
      })
    return result
  }
}

export const verifyMobileNumber = (userId, values) => {
  return async () => {
    let result = null
    const formData = values
    formData._method = 'PATCH'
    await axios
      .post(`${REACT_APP_API_URL}/users/${userId}`, formData)
      .then((res) => {
        result = res.data
      })
      .catch((e) => {
        const error = { ...e }
        result = error.response.data
      })
    return result
  }
}

export const updatePassword = (userId, values) => {
  return async () => {
    let result = null
    const formData = values
    formData._method = 'PATCH'
    await axios
      .post(`${REACT_APP_API_URL}/users/${userId}`, formData)
      .then((res) => {
        result = res.data
      })
      .catch((e) => {
        const error = { ...e }
        result = error.response.data
      })
    return result
  }
}

export const updateProgress = (progressStep, back = false) => {
  let progress = progressStep
  return (dispatch) => {
    if (back) {
      progress -= 1
    } else {
      progress += 1
    }
    dispatch({ type: setProgress, payload: progress })
    return progress
  }
}

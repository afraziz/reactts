import { createSlice } from '@reduxjs/toolkit'

export const RegTypes = {
  USER_ID: 0,
  PHONE_NUMBER: '',
  EMAIL: '',
  FIRST_NAME: '',
  LAST_NAME: '',
  PROGRESS_STEP: 1,
  TOTAL_STEPS: 5,
  HAS_NUMBER_MATCH: false,
  IS_EXISTING_SUPPLIER: false,
  SUPPLIER_CODE: '',
  ACCESS_TOKEN: null,
}

const registrationSlice = createSlice({
  name: 'registration',
  initialState: {
    userId: RegTypes.USER_ID,
    supplierCode: RegTypes.SUPPLIER_CODE,
    isExistingSupplier: RegTypes.IS_EXISTING_SUPPLIER,
    totalSteps: RegTypes.TOTAL_STEPS,
    progressStep: RegTypes.PROGRESS_STEP,
    mobile: RegTypes.PHONE_NUMBER,
    hasNumberMatch: RegTypes.HAS_NUMBER_MATCH,
    email: RegTypes.EMAIL,
    firstName: RegTypes.FIRST_NAME,
    lastName: RegTypes.LAST_NAME,
    accessToken: RegTypes.ACCESS_TOKEN,
  },
  reducers: {
    resetDetails(state) {
      return {
        ...state,
        userId: RegTypes.USER_ID,
        progressStep: RegTypes.PROGRESS_STEP,
        mobile: RegTypes.PHONE_NUMBER,
        email: RegTypes.EMAIL,
        firstName: RegTypes.FIRST_NAME,
        lastName: RegTypes.LAST_NAME,
        accessToken: RegTypes.ACCESS_TOKEN,
      }
    },
    setIsExistingSupplier(state, action) {
      return {
        ...state,
        isExistingSupplier: action.payload,
      }
    },
    setSupplierId(state, action) {
      return {
        ...state,
        supplierCode: action.payload,
      }
    },
    saveBusinessDetails(state, action) {
      return {
        ...state,
        userId: action.payload.id,
        firstName: action.payload.firstName,
        lastName: action.payload.lastName,
        email: action.payload.email,
        mobile: action.payload.mobile,
      }
    },
    saveAccessToken(state, action) {
      return {
        ...state,
        accessToken: action.payload,
      }
    },
    setProgress(state, action) {
      return {
        ...state,
        progressStep: action.payload,
      }
    },
  },
})

export const {
  resetDetails,
  setProgress,
  saveBusinessDetails,
  setIsExistingSupplier,
  setSupplierId,
  saveAccessToken,
} = registrationSlice.actions

export default registrationSlice.reducer
